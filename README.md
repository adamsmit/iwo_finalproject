# IWO_finalproject

This bitbucket contains the script that you can run to get the relevant information from the csv file. 
You can run the shellscript by putting the following commands into your terminal:

"chmod +x datascript.sh"

"./datascript.sh <datafile>"

In the datafile part of the second command line argument you insert one of the three csv files to see the information from one of the three years. 
