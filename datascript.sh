#!/bin/bash

# Shell script to analyze the data of the CSV file used for this research project.
# This script outputs only the lines that start with the number 4.

cat "$1" | grep ^4

